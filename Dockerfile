FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.17-alpine3.21@sha256:7a7d2c121e3c3d1502536107bce1d88e63347286570e7b02f83a35896fe5223f

ARG CI_COMMIT_SHORT_SHA

LABEL maintainer="Sylvain Desbureaux <sylvain.desbureaux@orange.com>" \
      name=infra_collection \
      version=$CI_COMMIT_SHORT_SHA \
      org.opencontainers.image.source="https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection" \
      org.opencontainers.image.ref.name="infra_collection" \
      org.opencontainers.image.authors="Sylvain Desbureaux <sylvain.desbureaux@orange.com>" \
      org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA

ENV APP /opt/infra/
ENV COLLECTION_PATH ~/.ansible/collections

WORKDIR $APP

COPY . $APP/

RUN mkdir -p "$COLLECTION_PATH" && \
    git config --global --add safe.directory "$PWD/.git" &&\
    ansible-galaxy collection install "git+file://$PWD/.git" \
      -p "$COLLECTION_PATH" && \
    rm -rf "$APP/.git" && \
    rm /root/.ansible/collections/ansible_collections/community/general/plugins/modules/java_keystore.py
