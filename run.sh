#!/bin/bash

# SPDX-license-identifier: Apache-2.0
##############################################################################
# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
##############################################################################

set -o errexit
set -o nounset
set -o pipefail

labels=$*

RUN_SCRIPT=${0}
RUN_ROOT=$(dirname $(readlink -f ${RUN_SCRIPT}))
export RUN_ROOT=$RUN_ROOT
source ${RUN_ROOT}/scripts/rc.sh

# register our handler
trap submit_bug_report ERR


#-------------------------------------------------------------------------------
# If no labels are set with args, run all
#-------------------------------------------------------------------------------
if [[ $labels = "" ]]; then
  labels="deploy_docker"
fi

#-------------------------------------------------------------------------------
# Install docker daemon
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_docker"* ]]; then
  step_banner "Install Docker daemon"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_docker.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Docker daemon installed"
fi

#-------------------------------------------------------------------------------
# test docker installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_docker"* ]]; then
  step_banner "test that Docker is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_docker.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Docker is working"
fi
