# Deploy docker

## Purpose

Install docker as container hosting daemon

## Inventory

The inventory must provide a group `docker` where docker daemon will be
installed.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
```

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose                      | Default value            |
|-----------------|------------------------------|--------------------------|
| `property_file` | property file to load        | `idf.yml`. Optional      |
| `base_dir`      | property file to load folder | `{{ inventory_dir }}/..` |

## Examples

Inventory:

```ini
[docker]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/deploy_docker.yml

ansible-playbook -i inventory/inventory playbooks/deploy_docker.yml \
    --extra-vars "base_dir=~"

ansible-playbook -i inventory/inventory playbooks/deploy_docker.yml \
    --extra-vars "{'docker_add_user': 'cloud'}"
```
