# APT install

## Purpose

Install in a safe way packages with apt.
As "apt" module of ansible doesn't check if another installation is in progress,
wrap it with some tasks making it safer to execute

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                       | Purpose                                | Default value                                 |
|--------------------------------|----------------------------------------|-----------------------------------------------|
| `proxy_env`                    | proxy environments to use              | `{}` (see <./proxy_env.md> for configuration) |
| `apt_install_retries`          | number of retries for installation     | `4`                                           |
| `apt_install_delay`            | delay between two retries              | `10` seconds                                  |
| `packages`                     | packages to install (deprecated)       | Not Set                                       |
| `apt_install_packages`         | packages to install                    | `[]`                                          |
| `apt_install_update_cache`     | do we update cache before installation | true                                          |
| `apt_install_cache_valid_time` | if we update cache, time of validity   | `3600` seconds                                |
| `apt_install_state`            | package target state                   | `present`                                     |
<!-- markdownlint-enable line-length -->

## Notes

`packages` is deprecated and will be removed in a further version.

## Examples

```yaml
# Install git and vim.nox
- ansible.builtin.include_role:
    name: orange.infra.apt_install
  vars:
    apt_install_packages:
      - git
      - vim.nox

# update all packages
- ansible.builtin.include_role:
    name: orange.infra.apt_install
  vars:
    apt_install_packages: '*'
    apt_install_state: latest
```
