# Create disk

## Purpose

Create a disk (filesystem + mount) on the right volume

## Requirements

The only requirement is to have a disk list which describe the purpose of the
disk in a server in this form:

```yaml
disks:
  - name: disk-XYZ
  - name: disk2
  - name: disk-db
```

server must use `systemd` for the services. (Should) works on `apt` and `yum`
based systems (debian, ubuntu, centos, rhel, …).

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                       | Purpose                                     | Default value                                 |
|--------------------------------|---------------------------------------------|-----------------------------------------------|
| `proxy_env`                    | proxy environments to use                   | `{}` (see <./proxy_env.md> for configuration) |
| `create_disk_disk_purpose`     | disk purpose                                | Not set                                       |
| `create_disk_mount_path`       | where to mount the disk                     | `/mnt`                                        |
| `create_disk_fstype`           | filesystem type to use                      | `xfs`                                         |
| `create_disk_force_full_erase` | wipe the disk before mounting               | `true`                                        |
| `create_disk_mount_options`    | mount options                               | `defaults,noatime`                            |
| `create_disk_copy_data`        | do we copy previous data into the new disk? | `false`                                       |
| `create_disk_state`            | state of the disk at the end                | `mounted`                                     |
<!-- markdownlint-enable line-length -->

### Purpose of the disk

This value is used to find the right disk in the list to choose.

On the example above, it can be 'db' to take the third disk.

### Filesystem types

Filesystems supported are `xfs`, `ext2` ,`ext3` ,`ext4`, `btrfs`.

### Deprecations

Here's the list of deprecated variables:

| Variable           | Purpose                                     |
|--------------------|---------------------------------------------|
| `disk_purpose`     | disk purpose                                |
| `mount_path`       | where to mount the disk                     |
| `fstype`           | filesystem type to use                      |
| `force_full_erase` | wipe the disk before mounting               |
| `mount_options`    | mount options                               |
| `copy_data`        | do we copy previous data into the new disk? |
| `state`            | state of the disk at the end                |

If set, they will erase their `create_disk_` counterpart.
Please move to the ones starting with `create_disk_` in front as the other ones
will be removed in a later release.

## Dependencies

`dd` is used on the target server and it then must also be present.

## Examples

```yaml
- hosts: servers
  pre_tasks:
    - set_fact:
        disks:
          - name: disk-XYZ
          - name: disk2
          - name: disk-db
  roles:
    - role: create_disk
      disks: "{{ disks }}"
      disk_purpose: db
      mount_path: /var/lib/db
```
