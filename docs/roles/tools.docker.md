# tools.docker role

## Purpose

This role installs docker (client + daemon) on a machine.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                          | Purpose                                                                | Default value                                                    |
|-----------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------|
| `proxy_env`                       | proxy environments to use                                              | `{}` (see <./proxy_env.md> for configuration)                    |
| `docker_add_user`                 | user to add in docker groups (will be allowed the use docker commands) | `user`                                                           |
| `docker_repository`               | docker repository to use                                               | <https://download.docker.com/linux>                              |
| `docker_repository_gpg`           | docker gpg key to use for docker repositories                          | `{{ docker_repository }}/{{ ansible_distribution.lower() }}/gpg` |
| `docker_debian_url`               | apt docker url to download docker package                              | `{{ docker_repository }}/{{ ansible_distribution.lower() }}`     |
| `docker_logrotate_size`           | number of logrotate file to keep for docker containers logs            | `7`                                                              |
| `docker_logrotate_number`         | max size before rotating for docker containers logs                    | `50M`                                                            |
| `docker_rhel_repository`          | docker repository to use for RHEL (deprecated)                         | `{{ docker_repository }}`                                        |
| `docker_rhel_url`                 | yum repo url to download docker package                                | stable url                                                       |
| `docker_rhel_gpg`                 | docker gpg key to use for docker yum repository  (deprecated)          | `{{ docker_repository_gpg }}/`                                   |
<!-- markdownlint-enable line-length -->
