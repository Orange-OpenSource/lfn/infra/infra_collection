# virtual environment

## Purpose

This role installs a Python virtual environment before installing
the Python packages defined in the `external_requirements` role
of a collection.

This role should be included into the `external_requirements` role
of the collection that will use it.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                            | Purpose                                         | Default value                                                      |
|-------------------------------------|-------------------------------------------------|--------------------------------------------------------------------|
| `virtualenv_dir`                    | directory under which the virtualenv is created | `{{ ansible_user_dir }}/.local/venv/{{ ansible_collection_name }}` |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.include_role:
    name: orange.k8s.virtualenv


- ansible.builtin.include_role:
    name: orange.k8s.virtualenv
  vars:
    virtualenv_dir: /var/lib/rancher/rke2/virtualenv
```
