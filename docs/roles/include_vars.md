# include vars

## Purpose

Check if a file is present on the Ansible controller.
If the file is present, load it for including variables set in it.

## Parameters

| Variable        | Purpose              | Default value             |
|-----------------|----------------------|----------------------------|
| `property_file` | the name of the file | `idf.yml`                  |
| `base_dir`      | the base directory   | `"{{ inventory_dir }}/.."` |

## Notes

the file will be searched in `{{ base_dir }}/vars`.

## Examples

```yaml
- ansible.builtin.include_role:
    name: orange.infra.include_vars
  vars:
    property_file: properties.yml
```
