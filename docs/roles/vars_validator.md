# vars validator

## Purpose

Check the given variables are set and are in the right format.
The format is given using [json schema](https://json-schema.org/) specification.

## Parameters

| Variable               | Purpose                            | Default value |
|------------------------|------------------------------------|---------------|
| `infra_mandatory_vars` | the list of specification to check | `[]`          |

## Specifications example

### a non empty string

```yaml
name: my_role_name
description: a description used in case of failure
schema:
  type: string
  minLength: 1
```

### a positive integer

```yaml
name: my_role_number
description: a description used in case of failure
schema:
  type: integer
  minimum: 0
```

### an array with a least one element

Items of the array can be validated also:

```yaml
name: my_role_list
description: a description used in case of failure
schema:
  type: array
  minItems: 1
  items:
    type: string
    minLength: 1
```

### a dictionary with required values

```yaml
name: my_role_object
description: a description used in case of failure
schema:
  type: object
  properties:
    flavor:
      type: string
      minLength: 1
    cpus:
      type: integer
      minimum: 1
    memory:
      type: string
      minLength: 1
    vendor:
      type: string
      minLength: 1
    model:
      type: string
      minLength: 1
  required:
    - model
    - vendor
```

## Examples

```yaml
- ansible.builtin.include_role:
    name: orange.infra.vars_validator
  vars:
    infra_mandatory_vars:
      - name: my_role_name
        description: a description used in case of failure
        schema:
          type: string
          minLength: 1
      - name: my_role_list
        description: a description used in case of failure
        schema:
          type: array
          minItems: 1
          items:
            type: string
            minLength: 1
```

an example with complex formats

```yaml
- ansible.builtin.include_role:
    name: orange.infra.vars_validator
  vars:
    infra_mandatory_vars:
      - name: os_infra_project_name
        description: the project we want to use
        schema:
          type: string
          minLength: 1
      - name: os_infra_user_name
        description: the user name to manage on openstack
        schema:
          type: string
          minLength: 1
      - name: os_infra_nodes_roles
        description: the list of servers with associated roles
        schema:
          type: object
          patternProperties:
            "^.*":
              description: the server name
              type: array
              minItems: 1
              items:
                description: the roles for the server
                type: string
                minLength: 1
      - name: os_infra_net_config
        description: the network configuration description
        schema:
          type: object
          patternProperties:
            "^.*":
              description: the network name
              type: object
              properties:
                network:
                  type: string
                  minLength: 1
                mask:
                  type: integer
                gateway:
                  type: string
                  minLength: 1
                dns:
                  type: string
                  minLength: 1
      - name: nodes
        description: the description of the machines
        schema:
          type: array
          items:
            type: object
            properties:
              name:
                type: string
                minLength: 1
              node:
                type: object
                properties:
                  flavor:
                    type: string
                    minLength: 1
                  cpus:
                    type: integer
                  memory:
                    type: string
                    minLength: 1
                  vendor:
                    type: string
                    minLength: 1
                  model:
                    type: string
                    minLength: 1
                oneOf:
                  - required:
                      - flavor
                  - required:
                      - cpus
                      - memory
                required:
                  - model
                  - vendor
              disks:
                type: array
                minItems: 1
                items:
                  type: object
                  properties:
                    name:
                      type: string
                      minLength: 1
                    disk_capacity:
                      type: string
                      minLength: 1
                    disk_type:
                      type: string
                    disk_interface:
                      type: string
                      minLength: 1
                  required:
                    - name
                    - disk_capacity
                    - disk_interface
            required:
              - name
              - node
              - disks
```
