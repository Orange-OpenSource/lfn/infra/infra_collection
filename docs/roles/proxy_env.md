# Proxy environments

All roles that may needs proxy configuration is using the same pattern:

```yaml
proxy_env:
  http_proxy: "{{ ansible_env['http_proxy'] | default('') }}"
  https_proxy: "{{ ansible_env['https_proxy'] | default('') }}"
  no_proxy: "{{ ansible_env['no_proxy'] | default('') }}"
```

If proxy configuration needs to be overwritten, then either provide
information in preconfigured environment variable in the target or give the
dictionary with the configuration.

Bear in mind that if you want to override only one or two of the 3 values,
you'll need to specify the three.

For example, if you just want to override `https_proxy`, you need to do:

```yaml
proxy_env:
  http_proxy: "{{ ansible_env['http_proxy'] | default('') }}"
  https_proxy: MY_PROXY
  no_proxy: "{{ ansible_env['no_proxy'] | default('') }}"
```

or

```yaml
proxy_env:
  http_proxy:
  https_proxy: MY_PROXY
  no_proxy:
```
