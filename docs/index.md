# Infra collection

The goal of this collection consists in infrastructure utilities.

It consists in several roles:

- apt_install
- create_disk
- include_vars
- tools.docker
- vars_validator
- virtualenv

A playbook is also provided in order to install a Docker daemon on servers.

## Playbooks
<!-- markdownlint-disable line-length -->
| Playbook                                      | Description                                              |
| --------------------------------------------- | -------------------------------------------------------- |
| [deploy_docker](./playbooks/deploy_docker.md) | Playbook to install Docker daemon on servers             |
| test_docker                                   | Playbook use for the collection gating (docker part)     |

## Plugin
<!-- markdownlint-disable line-length -->
| Plugin                                              | Description                                             |
| --------------------------------------------------- | ------------------------------------------------------- |
| [var validator](./plugins/modules/var_validator.md) | Plugin to validate variables according to a json schema |
<!-- markdownlint-enable line-length -->

## Roles
<!-- markdownlint-disable line-length -->
| Role                                        | Description                                          |
| ------------------------------------------- | ---------------------------------------------------- |
| [apt install](./roles/apt_install.md)       | Enhanced apt installer                               |
| [create disk](./roles/create_disk.md)       | Create Disk (filesystem + mount) on the right volume |
| [include vars](./roles/include_vars.md)     | Include vars/files                                   |
| [tools.docker](./roles/tools.docker.md)     | Install Docker daemon                                |
| [vars validator](./roles/vars_validator.md) | variable validation                                  |
| [virtualenv](./roles/virtualenv.md)         | Install a Python virtualenv                          |
<!-- markdownlint-enable line-length -->

## Versions

| ORONAP version | Collection sha1 |
| -------------- | --------------- |
| Honfleur       | N.A             |
| Istres         | N.A             |
