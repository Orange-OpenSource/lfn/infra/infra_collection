# Var validator

Validate that a variable satisfies the given schema.
The format is given using [json schema](https://json-schema.org/) specification.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable          | Required | Purpose                                            |
|-------------------|----------|----------------------------------------------------|
| `variablename`    | True     | the name of the variable                           |
| `description`     | False    | a description to be given in case of failure       |
| `schema`          | True     | the schema to validate against                     |
| `variablecontent` | True     | the content of the variable (what will be checked) |
<!-- markdownlint-enable line-length -->

### Return

Module will fail if variable is not validated against the schema.
If validation is ok, it will return a message saying it's OK.

### Examples

```yaml
- name: check the variable is a non empty string
  range.os_infra_manager.var_validator:
    variablename: my_variable
    description: used for creating a fact
    schema:
      type: string
      minLength: 1
    variablecontent: "a string"

- name: check the variable is an array of positive integer
  range.os_infra_manager.var_validator:
    variablename: my_variable
    description: used for creating a fact
    schema:
      type: array
      minItems: 1
      items:
        type: integer
        minimum: 0
    variablecontent:
      - 1
      - 2
      - 3
```
