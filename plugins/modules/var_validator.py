#!/usr/bin/python

# Copyright: (c) 2018, Sylvain Desbureaux <sylvain.desbureaux@orange.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import traceback
from ansible.module_utils.basic import AnsibleModule, missing_required_lib
__metaclass__ = type

DOCUMENTATION = r'''
---
module: var_validator
short_description: validate that a variable satisfies the given schema
version_added: "2.0.0"
author:
  - Sylvain Desbureaux (@sdesbureOL)
requirements:
  - "jsonschema (https://pypi.org/project/jsonschema/)"
description:
  - validate that a variable satisfies the given json schema
options:
  variablename:
    description:
      - the name of the variable
    required: true
    type: str
  description:
    description:
      - a description to be given in case of failure
    required: false
    type: str
  schema:
    description:
      - the schema to validate against (in jsonshema format)
    required: true
    type: dict
  variablecontent:
    description:
      - the content of the variable (what will be checked)
    required: true
    type: raw
'''

EXAMPLES = r'''
- name: check the variable is a non empty string
  range.os_infra_manager.var_validator:
    variablename: my_variable
    description: used for creating a fact
    schema:
      type: string
      minLength: 1
    variablecontent: "a string"

- name: check the variable is an array of positive integer
  range.os_infra_manager.var_validator:
    variablename: my_variable
    description: used for creating a fact
    schema:
      type: array
      minItems: 1
      items:
        type: integer
        minimum: 0
    variablecontent:
      - 1
      - 2
      - 3
'''

RETURN = r'''
message:
    description: The written result of the validation.
    returned: always
    type: str
    sample:
        - Variable 'my_variable' is valid
        - Bad file format in 'my_variable': 'jsonschema error'
        - Bad file format in 'my_variable': 'jsonschema error'
          Purpose of the variable:
            description given
'''

try:
    from jsonschema import validate
    from jsonschema.exceptions import ValidationError
    HAS_JSONSCHEMA = True
except ImportError:
    JSONSCHEMA_IMP_ERR = traceback.format_exc()
    HAS_JSONSCHEMA = False

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        variablecontent=dict(type='raw', required=True),
        schema=dict(type='dict', required=True),
        variablename=dict(type='str', required=True),
        description=dict(type='str', required=False),
    )

    # seed the result dict in the object
    result = dict(
        changed=False,
        message='',
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
    )

    if not HAS_JSONSCHEMA:
        module.fail_json(
            msg=missing_required_lib("jsonschema"),
            exception=JSONSCHEMA_IMP_ERR,
        )
    try:
        validate(
            instance=module.params['variablecontent'],
            schema=module.params['schema'],
        )
    except ValidationError as err:
        msg = "Bad file format in '{}': '{}'".format(
            module.params['variablename'],
            err.message,
        )
        if module.params['description']:
            msg += "\nPurpose of the variable:\n  {}".format(
                module.params['description'],
            )
        module.fail_json(msg=msg, changed=False)

    result['message'] = "Variable '{}' is valid".format(
        module.params['variablename'],
    )
    module.exit_json(**result)



def main():
    run_module()


if __name__ == '__main__':
    main()
