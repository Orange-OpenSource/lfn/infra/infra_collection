---
- name: check if using deprecated parameters
  ansible.builtin.include_tasks: deprecation.yml

- name: install Debian packages
  ansible.builtin.include_tasks: install_DEBIAN.yml
  when: ansible_os_family | lower == 'debian'

- name: install Red Hat packages
  ansible.builtin.include_tasks: install_RHEL.yml
  when: ansible_os_family | lower == 'redhat'

- name: retrieve disk information (lsscsi method)
  ansible.builtin.shell: "lsscsi | grep disk | awk '{print $7}'"
  register: list_disks

- name: use lsscsi method
  ansible.builtin.include_tasks: lsscsi.yml
  when: list_disks.stdout_lines | length > 0

- name: retrieve disk information (lshw method)
  ansible.builtin.shell: >-
    sudo lshw -class disk -json |
    jq -rs
    '. | if .[0]|type != "array" then . else .[] end | sort_by(.businfo)' |
    jq -r '.[] | select(.id != "cdrom") | .logicalname'
  become: true
  register: list_disks_lshw
  when: list_disks.stdout_lines | length == 0

- name: use lshw method
  ansible.builtin.include_tasks: lshw.yml
  when: list_disks.stdout_lines | length == 0 and
        list_disks_lshw.stdout_lines | length > 0

- name: get block device list via metadata
  ansible.builtin.uri:
    url: http://169.254.169.254/latest/meta-data/block-device-mapping
    return_content: true
  register: metadatas
  ignore_errors: true
  when: list_disks.stdout_lines | length == 0 and
        list_disks_lshw.stdout_lines | length == 0

- name: use metadatas method
  ansible.builtin.include_tasks: metadata.yml
  when: list_disks.stdout_lines | length == 0 and
        list_disks_lshw.stdout_lines | length == 0 and
        not metadatas.failed

- name: retrieve disk informations using path (ls /dev/disk/by-path method)
  ansible.builtin.shell: >
    ls -l /dev/disk/by-path/ | grep -v part | grep -v total | awk '{print $11}'
    | sed 's|../..|/dev|'
  register: list_disk_pathes
  when: list_disks.stdout_lines | length == 0 and
        list_disks_lshw.stdout_lines | length == 0 and
        metadatas.failed

- name: use path method
  ansible.builtin.include_tasks: dev.yml
  when: list_disks.stdout_lines | length == 0 and
        list_disks_lshw.stdout_lines | length == 0 and
        metadatas.failed and
        list_disk_pathes.stdout_lines | length > 0

- name: use facts method
  ansible.builtin.include_tasks: facts.yml
  when: list_disks.stdout_lines | length == 0 and
        list_disks_lshw.stdout_lines | length == 0 and
        metadatas.failed and
        list_disk_pathes.stdout_lines | length == 0

- name: show number of parent directories
  debug:
    msg: "# of parent dir is {{ create_disk_mount_path.split('/') | length }}"

- name: "create {{ create_disk_mount_path }} parent directory"
  become: true
  ansible.builtin.file:
    state: directory
    path: "{{ create_disk_mount_path.split('/')[0:-1] | join('/') }}"
  when: create_disk_mount_path.split('/') | length > 2

- name: "remove {{ create_disk_mount_path }} from fstab"
  ansible.posix.mount:
    state: absent
    path: "{{ create_disk_mount_path }}"
  become: true
  when: create_disk_force_full_erase|bool

- name: reload systemd
  ansible.builtin.systemd:
    daemon_reload: true
  become: true
  when: create_disk_force_full_erase|bool

- name: "unmount {{ create_disk_mount_path }}"
  ansible.posix.mount:
    state: unmounted
    path: "{{ create_disk_mount_path }}"
  become: true
  when: create_disk_force_full_erase|bool

- name: "create partition on device {{ device_path }}"
  community.general.parted:
    device: "{{ device_path }}"
    label: gpt
    state: present
    number: 1
  become: true

- name: "wipe first volume partition on {{ device_path }}"
  ansible.builtin.command: "dd if=/dev/zero of={{ device_path }}1 bs=1M count=1"
  changed_when: true
  become: true
  when: create_disk_force_full_erase|bool

- name: "create filesystem on {{ device_path }}1"
  community.general.filesystem:
    fstype: "{{ create_disk_fstype }}"
    dev: "{{ device_path }}1"
  register: filesystem
  become: true

- name: "gather partition facts"
  ansible.builtin.setup:
    gather_subset:
      - '!all'
      - hardware

- name: "retrieve partition UUID"
  ansible.builtin.set_fact:
    partition_uuid:
      "{{ ansible_devices[device_name].partitions[device_name ~ '1'].uuid }}"

- name: "(re)gather partition facts"
  ansible.builtin.setup:
    gather_subset:
      - '!all'
      - hardware
  when: not partition_uuid

- name: "(re)retrieve partition UUID"
  ansible.builtin.set_fact:
    partition_uuid:
      "{{ ansible_devices[device_name].partitions[device_name ~ '1'].uuid }}"
  when: not partition_uuid

- name: copy the data presents on the new partition
  when: >
        (create_disk_copy_data | bool) and
        (ansible_mounts | selectattr('device', 'eq', device_path ~ '1')
        | list | length == 0)
  become: true
  block:
    - name: add filesystem to temporary mount part
      ansible.posix.mount:
        path: "/tmp/{{ create_disk_mount_path.split('/')[-1] }}"
        src: "UUID={{ partition_uuid }}"
        fstype: "{{ create_disk_fstype }}"
        opts: "{{ create_disk_mount_options }}"
        state: ephemeral

    - name: copy data to the partition
      ansible.posix.synchronize:
        src: "{{ create_disk_mount_path }}"
        dest: "/tmp"
        rsync_opts: "-AX"
      delegate_to: "{{ inventory_hostname }}"

    - name: remove temporary mount
      ansible.posix.mount:
        path: "/tmp/{{ create_disk_mount_path.split('/')[-1] }}"
        state: absent

- name: add filesystem to mount part
  ansible.posix.mount:
    path: "{{ create_disk_mount_path }}"
    src: "UUID={{ partition_uuid }}"
    fstype: "{{ create_disk_fstype }}"
    opts: "{{ create_disk_mount_options }}"
    state: "{{ create_disk_state }}"
  become: true
