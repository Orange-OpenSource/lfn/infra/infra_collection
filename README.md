# Ansible Collection - orange.infra

A collection for basic roles aimed at simplifying work.

## Collection dependencies

This collection uses the following modules:

- `ansible.posix`
- `community.general`
- `community.docker`

## Roles

- [apt_install](docs/roles/apt_install.md) which wraps tasks around apt in order
  to have a reliable installation;
- [create_disk](docs/roles/create_disk.md), which will mount / partition disks
  for Virtual machines according to simple information.
- [include_vars](docs/roles/include_vars.md), which check if a file exists on
  the controller and loads it if present
- [tools.docker](docs/roles/tools.docker.md), which install Docker daemon on
  servers
- [vars_validator](docs/roles/vars_validator.md), which validates that variables
  given in a list are following given schemas.

## Modules

- [var_validator](docs/plugins/modules/var_validator.md), which validates that
  a given variable is following a json schema.

## Using this collection

Before using the General community collection, you need to install the
collection with the `ansible-galaxy` CLI:

```shell
ansible-galaxy collection install https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection.git
```

use `,branch_or_tag` if you want to use a specific branch version:

```shell
ansible-galaxy collection install https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection.git,v1
```

You can also include it in a `requirements.yml` file and install it via
`ansible-galaxy collection install -r requirements.yml` using the format:

```yaml
collections:
  - name: git+https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection.git
    type: git
    version: master
```
