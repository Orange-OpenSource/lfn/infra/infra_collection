## [9.0.37](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.36...9.0.37) (2025-02-15)


### Bug Fixes

* **deps:** pin dependencies ([c30d8eb](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/c30d8ebdac0beb86fb658610f1eaaa03eaec7b7c))

## [9.0.36](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.35...9.0.36) (2025-02-15)


### Bug Fixes

* **deps:** update dependency community.docker to >=4.4.0,<4.5.0 ([676ceb6](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/676ceb6b46a72adf125c6a9d70637a2eab2c46c1))

## [9.0.35](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.34...9.0.35) (2025-02-13)


### Bug Fixes

* **⬆️:** bump to alpine 3.21 ([d7d15ef](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/d7d15efa4ae9a4545fb44ccbe0a11c047a287b6d))
* **deps:** update dependency community.general to >=10.3.1,<10.4.0 ([e3b4b25](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/e3b4b253bb369276d4afa64942cc38b704d4f0d4))

## [9.0.34](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.33...9.0.34) (2025-01-08)


### Bug Fixes

* **deps:** update dependency community.docker to >=4.3.0,<4.4.0 ([f65a34b](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/f65a34b47bedb073973c3d219ce4316f6d164c6c))

## [9.0.33](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.32...9.0.33) (2025-01-07)


### Bug Fixes

* **deps:** update dependency community.general to >=10.2.0,<10.3.0 ([e2db62b](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/e2db62b05c93b3099d9c555b053788158310845b))

## [9.0.32](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.31...9.0.32) (2024-12-17)


### Bug Fixes

* **deps:** update dependency community.docker to >=4.2.0,<4.3.0 ([cbd81ad](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/cbd81adf5f0b67df08f9cd2c428554e37538e07e))

## [9.0.31](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.30...9.0.31) (2024-12-13)


### Bug Fixes

* **deps:** update dependency ansible.posix to v2 ([7dddede](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/7dddede27668f7372802b76f26e3b0a3abe798cf))

## [9.0.30](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.29...9.0.30) (2024-12-13)


### Bug Fixes

* **deps:** update dependency community.docker to >=4.1.0,<4.2.0 ([1642a36](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/1642a36000ea81524a809624f431b4f39a549e0c))

## [9.0.29](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.28...9.0.29) (2024-12-13)


### Bug Fixes

* **deps:** update dependency community.general to >=10.1.0,<10.2.0 ([b5a8067](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/b5a8067b172b43982b88bf95265a4d7db4393c7d))

## [9.0.28](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.27...9.0.28) (2024-12-13)


### Bug Fixes

* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core docker tag to v2.17 ([d5c6646](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/d5c664646d03aae05247f04969ad830143b203b0))

## [9.0.27](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.26...9.0.27) (2024-11-19)


### Bug Fixes

* **deps:** update dependency community.general to v10 ([42f2b2b](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/42f2b2b4e5954ce5178a3c820e217f66afea9a2b))

## [9.0.26](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.25...9.0.26) (2024-10-21)


### Bug Fixes

* **deps:** update dependency community.docker to v4 ([9ed5e12](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/9ed5e1294861e42559bd40256c6cc66bd2112009))

## [9.0.25](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.24...9.0.25) (2024-10-17)


### Bug Fixes

* **deps:** update dependency community.general to >=9.5.0,<9.6.0 ([a879784](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/a8797842c27d8240a264cd235a8fc290b1786af3))

## [9.0.24](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.23...9.0.24) (2024-09-26)


### Bug Fixes

* update shebang of module accordingly to ansible doc ([7d297bb](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/7d297bbb675822485f8fad3493297676f2d81cc1))

## [9.0.23](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.22...9.0.23) (2024-09-23)


### Bug Fixes

* **deps:** update dependency community.general to >=9.4.0,<9.5.0 ([16543ea](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/16543ea77c2bef68eefb9f395b969bece4093468))

## [9.0.22](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.21...9.0.22) (2024-09-23)


### Bug Fixes

* remove dubious ownership detection ([e716c47](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/e716c47416cd7b69945d2a1ca3c39762144875e1))

## [9.0.21](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.20...9.0.21) (2024-08-26)


### Bug Fixes

* **deps:** update dependency community.general to >=9.2.0,<9.3.0 ([0af7aa9](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/0af7aa9743744a3df0170618a437e294fc5b1bdd))

## [9.0.20](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.19...9.0.20) (2024-06-18)


### Bug Fixes

* **deps:** update dependency community.general to >=9.1.0,<9.2.0 ([5832bbe](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/5832bbe4017395f82896c48491001da83f186bc0))

## [9.0.19](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.18...9.0.19) (2024-05-21)


### Bug Fixes

* **deps:** update dependency community.general to v9 ([cd40c0d](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/cd40c0dbce4b919e10e1222028d49e37ae12efd8))

## [9.0.18](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.17...9.0.18) (2024-04-29)


### Bug Fixes

* **deps:** update dependency community.general to >=8.6.0,<8.7.0 ([f32f4b9](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/f32f4b9c058106b671d98d441bc5ef162b81ce96))

## [9.0.17](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.16...9.0.17) (2024-03-26)


### Bug Fixes

* **deps:** update dependency community.general to >=8.5.0,<8.6.0 ([63ec5c0](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/63ec5c098e08de26253fd036c610fae66fa07e42))

## [9.0.16](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.15...9.0.16) (2024-03-11)


### Bug Fixes

* **deps:** update dependency community.general to >=8.4.0,<8.5.0 ([99d70af](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/99d70af90d78ef1b1e7eced64004c9d8b364bad9))

## [9.0.15](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.14...9.0.15) (2024-1-30)


### Bug Fixes

* **deps:** update dependency community.general to >=8.3.0,<8.4.0 ([8cafe8c](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/8cafe8cee4ee2ec030e59f3bfa4f7b67fff3c2bd))

## [9.0.14](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.13...9.0.14) (2024-01-09)


### Bug Fixes

* **deps:** update dependency community.general to >=8.2.0,<8.3.0 ([233a706](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/233a706aa3c17dd79f7ec1752e29bbbf867bd791))

## [9.0.13](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.12...9.0.13) (2023-12-05)


### Bug Fixes

* **deps:** update dependency community.general to >=8.1.0,<8.2.0 ([19dff61](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/19dff61c210981e4ee82bb284a7e2cb9124787ba))

## [9.0.12](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.11...9.0.12) (2023-11-13)


### Bug Fixes

* **deps:** update dependency community.general to v8 ([245a446](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/245a446e1ca7e1255420a66049e76d51ba272143))
* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core docker tag to v2.16 ([22e5dc8](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/22e5dc85bcb4d16788e9bd87414d3a09b5ac7744))

## [9.0.11](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.10...9.0.11) (2023-09-13)


### Bug Fixes

* **deps:** update dependency community.general to >=7.4.0,<7.5.0 ([8bd0a1a](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/8bd0a1a689181ea956d308f19e0bcdb9bd018817))

## [9.0.10](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.9...9.0.10) (2023-08-30)


### Bug Fixes

* **deps:** update dependency community.general to >=7.3.0,<7.4.0 ([7b3824a](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/7b3824a73be8962931636da30263ddc2bcea4271))

## [9.0.9](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.8...9.0.9) (2023-07-18)


### Bug Fixes

* **deps:** update dependency community.general to >=7.2.0,<7.3.0 ([2ad046c](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/2ad046c3900a6e0089573878cb34c8f4af7b9032))

## [9.0.8](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.7...9.0.8) (2023-06-26)


### Bug Fixes

* **deps:** update dependency community.general to >=7.1.0,<7.2.0 ([3439bb5](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/3439bb52e70c1ef957ac3b2ea0244d4b85fda43e))

## [9.0.7](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.6...9.0.7) (2023-05-16)


### Bug Fixes

* **deps:** update dependency community.general to v7 ([b2e02f0](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/b2e02f0be02d619893762479784aeba5ab6a860b))

## [9.0.6](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.5...9.0.6) (2023-05-04)


### Bug Fixes

* **deps:** update dependency community.general to >=6.6.0,<6.7.0 ([2e4ee74](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/2e4ee74b78f535556afb35fe7751d673a9030ade))

## [9.0.5](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.4...9.0.5) (2023-03-28)


### Bug Fixes

* **deps:** update dependency community.general to >=6.5.0,<6.6.0 ([f781872](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/f781872912d61fd6b0a94b4588cfa5f26657f478))

## [9.0.4](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.3...9.0.4) (2023-02-28)


### Bug Fixes

* **deps:** update dependency community.general to >=6.4.0,<6.5.0 ([c1a09ca](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/c1a09cae8fbbdde285e6f411667789b4184a69cc))

## [9.0.3](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/compare/9.0.2...9.0.3) (2023-02-03)


### Bug Fixes

* **deps:** update dependency community.general to >=6.3.0,<6.4.0 ([257e7f7](https://gitlab.com/Orange-OpenSource/lfn/infra/infra_collection/commit/257e7f731ad3f6418b3a332940bd941fe4516c9e))
